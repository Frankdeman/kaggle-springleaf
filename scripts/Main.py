# Import statements
import os
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
import sklearn.metrics as metrics
import numpy as np

def selectArtifactFreeIndices(data):

    ori_index = data.index[:]

    for colnaam in data.columns[0:-1]:

        # Remove artifacts based on IQR per colom
        Q1 = data[colnaam].quantile(0.25)
        Q3 = data[colnaam].quantile(0.75)

        # Select the indices with outliers or NaNs
        ind_good = data.index[
            (data[colnaam].between(Q1 - 3 * Q1, Q3 + 3 * Q3)) &
            (data[colnaam].isnull() == False)]

        if colnaam == data.columns[0]:
            ind_wrong = np.setdiff1d(ori_index, ind_good)

        else:
            ind_wrong = np.append(ind_wrong, np.setdiff1d(ori_index, ind_good))

    ind_wrong = np.unique(ind_wrong)

    data = data.drop(ind_wrong.tolist())

    return data

    #data = pd.read_csv(full_path, usecols=colnames[1:10] + [colnames[-1]])
    #data_train = data.iloc[all_ind_good]

# Load the training file
script_dir = os.path.dirname(__file__)  # Script directory
full_path = os.path.join(script_dir, '../data/train.csv')

# Determine the column names with solely numbers as input
data = pd.read_csv(full_path, chunksize=20)
colnames = list(data.get_chunk(1).select_dtypes(exclude=[object, bool]).columns)
colnames = colnames[1:15]
data = pd.read_csv(full_path, usecols=colnames + ['target'])

# Remove artifacts based on the IQR
data_train = selectArtifactFreeIndices(data)

# Create logistic regression model
lr = LogisticRegression(random_state=0).fit(data_train.iloc[:,range(0,len(data_train.columns)-1)], data_train.target)

# Calculate the probabilities
probs = lr.predict_proba(data_train.iloc[:,range(0,len(data_train.columns)-1)])
fpr, tpr, threshold = metrics.roc_curve(data_train.target, probs[:, 1])
roc_auc = metrics.auc(fpr, tpr)

# Plot an ROC curve
plt.title('Receiver Operating Characteristic')
plt.plot(fpr, tpr, 'b', label='AUC = %0.2f' % roc_auc)
plt.legend(loc='lower right')
plt.plot([0, 1], [0, 1], 'r--')
plt.xlim([0, 1])
plt.ylim([0, 1])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.show()
